//
//  AVRSSListModel.swift
//  RSSReaderTestApp
//
//  Created by Family Mac on 5/11/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import UIKit
import CoreData

/// AVRSSItemsViewer describes the set of methods to be implemented by Articles Viewer
protocol AVRSSItemsViewer : class
{
	/// Prepare for model updates
	func willStartUpdates()

	/// Model finished updates
	func didEndUpdates()

	/// Indicates about occured error
	func didOccuredError(_ error: AVRSSItemProviderError)
	
	/// Methods, that relates to the behavddeiour of model update
	func didInsertItem(at indexPath: IndexPath?)
	func didDeleteItem(at indexPath: IndexPath?)
	func didUpdateItem(at indexPath: IndexPath?)
	func didMoveItem(at sourceIndexPath: IndexPath?, to destinationIndexPath: IndexPath?)
}

class AVRSSListModel: NSObject, NSFetchedResultsControllerDelegate, AVRSSItemsProviderDelegate
{
	weak var RSSlistViewer: AVRSSItemsViewer?
	let RSSItemsProvider = AVRSSItemsProvider()
	var managedObjectContext: NSManagedObjectContext? = nil
	
	convenience init(aManagedObjectContext: NSManagedObjectContext?)
	{
		self.init()
		self.managedObjectContext = aManagedObjectContext
		self.RSSItemsProvider.delegate = self		
	}
	
	/// Hides item from user
	func deleteItem(at indexPath: IndexPath)
	{
		self.fetchedResultsController.object(at: indexPath).removedByUser = true
		let theContext = self.fetchedResultsController.managedObjectContext
		do
		{
		  try theContext.save()
		}
		catch
		{
			fatalError("Unresolved error")
		}
	}
	
	func RSSItem(at indexPath: IndexPath) -> AVRSSItem
	{
		return self.fetchedResultsController.object(at: indexPath)
	}
	
	func entitiesCount() -> Int
	{
		let theSectionInfo = self.fetchedResultsController.sections![0]
		return theSectionInfo.numberOfObjects
	}

	/// Initiates updating items, if there are no already scheduled updates
	func refreshEntitiesList()
	{
		if !self.RSSItemsProvider.itemsRefreshInProgress
		{
			self.RSSItemsProvider.refreshRSSFeed()
		}
	}

	private func synchronizeModel(RSSItems: [[String : AnyObject]])
	{
		//1. Add new items, that are not stored in Core Data
		let theContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
		theContext.parent = self.fetchedResultsController.managedObjectContext
		theContext.perform
		{
			var theStoredRSSItems = [AVRSSItem]()
			do
			{
			  try theStoredRSSItems = theContext.fetch(NSFetchRequest.init(entityName: "RSSItem"))
			}
			catch
			{
				fatalError("Unresolved error")
			}
		
			for theRawRSSItem in RSSItems
			{
				var theItemIsPresentInStore = false
				for theStoredRSSItem in theStoredRSSItems
				{
					theItemIsPresentInStore = theStoredRSSItem.equals(to: theRawRSSItem)
					if theItemIsPresentInStore
					{
						break
					}
				}
				if !theItemIsPresentInStore
				{
					let theItemToStore = NSEntityDescription.insertNewObject(forEntityName:
								"RSSItem", into: theContext) as! AVRSSItem
					theItemToStore.title = theRawRSSItem[AVRSSElementsName.title.rawValue] as? String
					theItemToStore.articleURL = theRawRSSItem[AVRSSElementsName.link.rawValue] as? String
					theItemToStore.date = theRawRSSItem[AVRSSElementsName.date.rawValue] as? NSDate
					theItemToStore.removedByUser = false
				}
			}
			
			//2. Remove items, that are marked as "removedByUser" and do not exists in server response
			
			for theStoredRSSItem in theStoredRSSItems
			{
				if theStoredRSSItem.removedByUser
				{
					var theItemIsPresentInRespose = false
					for theRawRSSItem in RSSItems
					{
						theItemIsPresentInRespose = theStoredRSSItem.equals(to: theRawRSSItem)
						if theItemIsPresentInRespose
						{
							break
						}
					}
					if (!theItemIsPresentInRespose)
					{
						theContext.delete(theStoredRSSItem)
					}
				}
			}
			do
			{
			  try theContext.save()
			}
			catch
			{
				fatalError("Unresolved error")
			}
		}
	}
	
// MARK: - AVRSSItemsProviderDelegate

	func didRefresh(RSSItems: [[String : AnyObject]]?, error: AVRSSItemProviderError?)
	{
		if let theError = error
		{
			self.RSSlistViewer?.didOccuredError(theError)
			//Show error
		}
		else if let theItems = RSSItems
		{
			self.synchronizeModel(RSSItems: theItems)
		}
	}

// MARK: - Fetched results controller
	
	private var fetchedResultsController: NSFetchedResultsController<AVRSSItem>
	{
		if _fetchedResultsController != nil
		{
			return _fetchedResultsController!
		}
		
		let theFetchRequest: NSFetchRequest<AVRSSItem> = AVRSSItem.fetchRequest()

		// Set the batch size to a suitable number.
		theFetchRequest.fetchBatchSize = 20

		// Edit the sort key as appropriate.
		let theSortDescriptor = NSSortDescriptor(key: "date", ascending: false)
		theFetchRequest.sortDescriptors = [theSortDescriptor]
	
		theFetchRequest.predicate = NSPredicate(format: "removedByUser = false")
		// Edit the section name key path and cache name if appropriate.
		// nil for section name key path means "no sections".
		let theFetchedResultsController = NSFetchedResultsController(fetchRequest: theFetchRequest,
					managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil,
					cacheName: "AVNewsList")
		theFetchedResultsController.delegate = self
		_fetchedResultsController = theFetchedResultsController
		
		do
		{
			try _fetchedResultsController!.performFetch()
		}
		catch
		{
			fatalError("Unresolved error")
		}
		
		return _fetchedResultsController!
	}
	private var _fetchedResultsController: NSFetchedResultsController<AVRSSItem>? = nil

	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
	{
		self.RSSlistViewer?.willStartUpdates()
	}

	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
	{
		self.RSSlistViewer?.didEndUpdates()
	}

	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange
				anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
				newIndexPath: IndexPath?)
	{
		switch type
		{
			case .insert:
				self.RSSlistViewer?.didInsertItem(at: newIndexPath)
			case .delete:
				self.RSSlistViewer?.didDeleteItem(at: indexPath)
			case .update:
				self.RSSlistViewer?.didUpdateItem(at: indexPath)
			case .move:
				self.RSSlistViewer?.didMoveItem(at: indexPath, to: newIndexPath)
	    }
	}
}
