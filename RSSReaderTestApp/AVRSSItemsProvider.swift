//
//  AVRSSItemsProvider.swift
//  RSSReaderTestApp
//
//  Created by Family Mac on 5/11/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import UIKit

enum AVRSSItemProviderError: Error
{
   case other
	case badConnection
	case badRespose
	
	/// Indicates, if given NSError is bad connection-related
	static func isBadConnection(error: NSError) -> Bool
	{
		let theNeedsToRepeatErrors = [NSURLErrorUnknown, NSURLErrorTimedOut,
				NSURLErrorCannotConnectToHost, NSURLErrorNetworkConnectionLost,
				NSURLErrorDNSLookupFailed, NSURLErrorNotConnectedToInternet,
				NSURLErrorCallIsActive,NSURLErrorDataNotAllowed,
				NSURLErrorCannotLoadFromNetwork]

		return NSNotFound != theNeedsToRepeatErrors.index(of: error.code);
	}
}

enum AVRSSElementsName: String
{
	case item = "item"
	case title = "title"
	case date = "pubDate"
	case link = "link"
}

protocol AVRSSItemsProviderDelegate : class
{
	func didRefresh(RSSItems: [[String : AnyObject]]?, error: AVRSSItemProviderError?)
}

class AVRSSItemsProvider: NSObject, XMLParserDelegate
{
	weak var delegate : AVRSSItemsProviderDelegate?
	var itemsRefreshInProgress = false
	
	private var RSSFeederParser : XMLParser?
	private var RSSItems = [[String : AnyObject]]()
	private var currentRSSItem = [String : AnyObject]()
	private var currentXMLElement = String()
	private var dataTask : URLSessionDataTask?
	
	/// Starts Feed refreshing. Calls didRefresh(RSSItems:,error:) when finish
	func refreshRSSFeed()
	{
		self.itemsRefreshInProgress = true
		let theFeedURLPath = "https://developer.apple.com/news/rss/news.rss"
		let theFeedURL = URL(string: theFeedURLPath)
		let theSessionConfiguration = URLSessionConfiguration.default
		let theSession = URLSession(configuration: theSessionConfiguration)
		let theRequest = URLRequest(url: theFeedURL!)
		self.dataTask = theSession.dataTask(with: theRequest, completionHandler: self.dataTaskHandler)
		self.dataTask?.resume()
	}
	
	func dataTaskHandler(data: Data?, response: URLResponse?, error: Error?)
	{
		if let theError = error
		{
			self.itemsRefreshInProgress = false
			let theSendingError : AVRSSItemProviderError =
						AVRSSItemProviderError.isBadConnection(error: theError as NSError) ?
						.badConnection : .other
			self.delegate?.didRefresh(RSSItems: nil, error: theSendingError)
		}
		else if let theData = data
		{
			self.RSSFeederParser = XMLParser(data: theData)
			self.RSSFeederParser?.delegate = self
			self.RSSItems.removeAll()
			self.RSSFeederParser?.parse()
		}
	}

// MARK: - XMLParserDelegate

	func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error)
	{
		self.itemsRefreshInProgress = false
		self.delegate?.didRefresh(RSSItems: nil, error: .badRespose)
	}

	func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI:
				String?, qualifiedName qName: String?, attributes attributeDict:
				[String : String] = [:])
	{
		self.currentXMLElement = elementName
		if (AVRSSElementsName.item.rawValue == elementName)
		{
			self.currentRSSItem = [String : AnyObject]()
		}
	}
	
	func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI:
				String?, qualifiedName qName: String?)
	{
		self.currentXMLElement = String()
		if elementName == AVRSSElementsName.item.rawValue && nil != self.currentRSSItem[AVRSSElementsName.title.rawValue] &&
					nil != self.currentRSSItem[AVRSSElementsName.link.rawValue]
		{
			if let theDateString = self.currentRSSItem[AVRSSElementsName.date.rawValue] as? String
			{
				let theDateFormater = DateFormatter()
				theDateFormater.dateFormat = "EEE, d MMM yyyy HH:mm:ss zzz"
				theDateFormater.locale = Locale(identifier: "en_US")
				
				if let theDate = theDateFormater.date(from: theDateString)
				{
					self.currentRSSItem[AVRSSElementsName.date.rawValue] = theDate as AnyObject!
				}
			}
			self.RSSItems.append(self.currentRSSItem)
		}
	}
	
	func parser(_ parser: XMLParser, foundCharacters string: String)
	{
		if self.currentXMLElement == AVRSSElementsName.title.rawValue ||
					self.currentXMLElement == AVRSSElementsName.link.rawValue ||
					self.currentXMLElement == AVRSSElementsName.date.rawValue
		{
			let theExtendedString : String
			if let theCurrentValue = self.currentRSSItem[self.currentXMLElement] as? String
			{
				// XML parser may parse current element characters by chunks
				// Thus check, if part of string was previously saved for currentXMLElement
				theExtendedString = theCurrentValue + string
			}
			else
			{
				theExtendedString = string
			}
			self.currentRSSItem[self.currentXMLElement] = theExtendedString as AnyObject?
		}
	}
	
	func parserDidEndDocument(_ parser: XMLParser)
	{
		self.currentXMLElement = String()
		self.currentRSSItem = [String : AnyObject]()
		self.itemsRefreshInProgress = false
		self.delegate?.didRefresh(RSSItems: self.RSSItems, error: nil)
	}
}
