//
//  AVArticleViewController.swift
//  RSSReaderTestApp
//
//  Created by Family Mac on 5/11/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import UIKit

class AVArticleViewController: UIViewController, UIWebViewDelegate, AVDynamicTypeSizesSupport
{
	@IBOutlet weak var webView: UIWebView!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var undeterminateProgress: UIActivityIndicatorView!
	
	/// URL for the Article to be shown
	var articleURL: URL?	
	
	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		self.webView.stopLoading()
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		if let theURL = self.articleURL
		{
			let theRequest = URLRequest(url: theURL)
			self.webView.loadRequest(theRequest)
			self.descriptionLabel.alpha = 0.0
			self.webView.alpha = 1.0
			self.webView.delegate = self
		}
		else
		{
			self.descriptionLabel.alpha = 1.0
			self.webView.alpha = 0.0
		}
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		self.subscribeForFontSizeChanges()
		super.viewDidAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool)
	{
		self.unsubscribeForFontSizeChanges()
		super.viewWillDisappear(animated)
	}
	
	private func stopIndeterminateProgress()
	{
		self.undeterminateProgress.stopAnimating()
		UIView.animate(withDuration: 0.5)
		{
			self.undeterminateProgress.alpha = 0.0
		}
	}
	
// MARK: - UIWebViewDelegate protocol implementation
	
	func webViewDidStartLoad(_ webView: UIWebView)
	{
		self.undeterminateProgress.startAnimating()
		UIView.animate(withDuration: 0.5)
		{
			self.undeterminateProgress.alpha = 1.0
		}
	}
	
	func webViewDidFinishLoad(_ webView: UIWebView)
	{
		self.stopIndeterminateProgress()
	}
	
	func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
	{
		self.stopIndeterminateProgress()
		let theAlert = UIAlertController(title: "Couldn't load the article", message:
					"Check your Internet Connection or try again later", preferredStyle: .alert)
		theAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		self.present(theAlert, animated: true, completion: nil)
	}
	
// MARK: - AVDynamicTypeSizesSupport protocol implementation
	
	func didUpdateDynamicTypeSizes()
	{
		self.descriptionLabel.font = UIFont.preferredFont(forTextStyle: .body)
	}
}

