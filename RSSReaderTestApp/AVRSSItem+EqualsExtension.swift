//
//  File.swift
//  RSSReaderTestApp
//
//  Created by Family Mac on 23/11/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import Foundation

// Is Equal for AVRSSItem and RAW dictionary

extension AVRSSItem
{
	func equals(to dictionary: [String : AnyObject]) -> Bool
	{
		return dictionary[AVRSSElementsName.title.rawValue] as? String == self.title &&
					dictionary[AVRSSElementsName.link.rawValue] as? String == self.articleURL &&
					dictionary[AVRSSElementsName.date.rawValue] as? NSDate == self.date
	}
}
