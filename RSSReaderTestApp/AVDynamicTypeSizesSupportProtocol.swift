//
//  AVDynamicTypeSizesSupportProtocol.swift
//  RSSReaderTestApp
//
//  Created by Family Mac on 6/11/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import UIKit

/// Defines Methods to be implemented by ViewControllers that should support Dynamic Type Sizes
protocol AVDynamicTypeSizesSupport
{
	/// Subscribes for Dynamic Type Sizes - related Notification
	func subscribeForFontSizeChanges()

	/// Unsubscribes for Dynamic Type Sizes - related Notification
	func unsubscribeForFontSizeChanges()

	/// Method that is called when Dynamic Type Sizes changes
	func didUpdateDynamicTypeSizes()
}

extension AVDynamicTypeSizesSupport where Self : UIViewController
{
	func subscribeForFontSizeChanges()
	{
		NotificationCenter.default.addObserver(forName:
					Notification.Name.UIContentSizeCategoryDidChange, object: nil, queue:
					OperationQueue.main)
		{ (Notification) in
			self.didUpdateDynamicTypeSizes()
		}
	}
	
	func unsubscribeForFontSizeChanges()
	{
		NotificationCenter.default.removeObserver(self, name:
					Notification.Name.UIContentSizeCategoryDidChange, object: nil)
	}
}
