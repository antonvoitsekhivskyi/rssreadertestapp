//
//  AppDelegate.swift
//  RSSReaderTestApp
//
//  Created by Family Mac on 5/11/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate
{
	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
				[UIApplicationLaunchOptionsKey: Any]?) -> Bool
	{
		let theSplitViewController = self.window!.rootViewController as! UISplitViewController
		let theNavigationController = theSplitViewController.viewControllers[
					theSplitViewController.viewControllers.count - 1] as! UINavigationController
		theNavigationController.topViewController!.navigationItem.leftBarButtonItem =
					theSplitViewController.displayModeButtonItem
		theSplitViewController.delegate = self

		let theMasterNavigationController = theSplitViewController.viewControllers[0] as!
					UINavigationController
		let theController = theMasterNavigationController.topViewController as! AVRSSTableListViewer
		theController.managedObjectContext = self.managedObjectContext
		return true
	}

	func applicationWillResignActive(_ application: UIApplication)
	{

	}

	func applicationDidEnterBackground(_ application: UIApplication)
	{

	}

	func applicationWillEnterForeground(_ application: UIApplication)
	{
	
	}

	func applicationDidBecomeActive(_ application: UIApplication)
	{

	}

	func applicationWillTerminate(_ application: UIApplication)
	{
		self.saveContext()
	}

// MARK: - Split view

	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary
				secondaryViewController: UIViewController, onto primaryViewController: UIViewController)
				-> Bool
	{
		guard let theSecondaryAsNavController = secondaryViewController as? UINavigationController
		else
		{
			return false
		}
		guard let theTopAsDetailController = theSecondaryAsNavController.topViewController as?
					AVArticleViewController else { return false }
		if theTopAsDetailController.articleURL == nil
		{
			// Return true to indicate that we have handled the collapse by doing nothing; the 
			// secondary controller will be discarded.
			return true
		}
		return false
	}
	
// MARK: - Core Data stack

	lazy var applicationDocumentsDirectory: URL =
	{
	    let theURLs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
	    return theURLs[theURLs.count - 1]
	}()

	lazy var managedObjectModel: NSManagedObjectModel =
	{
	    let theModelURL = Bundle.main.url(forResource: "RSSReaderTestApp", withExtension: "momd")!
	    return NSManagedObjectModel(contentsOf: theModelURL)!
	}()

	lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator =
	{
		// Create the coordinator and store
		let theCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
		let theURL = self.applicationDocumentsDirectory.appendingPathComponent(
			"RSSReaderTestApp.sqlite")
		var theFailureReason = "There was an error creating or loading the application's saved data."
		do
		{
			try theCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil,
						at: theURL, options: nil)
		}
		catch
		{
			// Report any error we got.
			var theDict = [String: AnyObject]()
			theDict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as
						AnyObject?
			theDict[NSLocalizedFailureReasonErrorKey] = theFailureReason as AnyObject?

			theDict[NSUnderlyingErrorKey] = error as NSError
			let theWrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: theDict)
			// Replace this with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use 
			// this function in a shipping application, although it may be useful during development.
			NSLog("Unresolved error \(theWrappedError), \(theWrappedError.userInfo)")
			abort()
		}
		
		return theCoordinator
	}()

	lazy var managedObjectContext: NSManagedObjectContext =
	{
	    // Returns the managed object context for the application (which is already bound to the 
		 // persistent store coordinator for the application.) This property is optional since there 
		 // are legitimate error conditions that could cause the creation of the context to fail.
	    let theCoordinator = self.persistentStoreCoordinator
	    var theManagedObjectContext = NSManagedObjectContext(concurrencyType:
					 .mainQueueConcurrencyType)
	    theManagedObjectContext.persistentStoreCoordinator = theCoordinator
	    return theManagedObjectContext
	}()

// MARK: - Core Data Saving support

	func saveContext ()
	{
		if managedObjectContext.hasChanges
		{
			do
			{
				try managedObjectContext.save()
			}
			catch
			{
				let nserror = error as NSError
				NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
				abort()
			}
		}
	}
}

