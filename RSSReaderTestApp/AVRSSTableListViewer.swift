//
//  AVRSSTableListViewer.swift
//  RSSReaderTestApp
//
//  Created by Family Mac on 5/11/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import UIKit
import CoreData

class AVRSSTableListViewer: UITableViewController, AVRSSItemsViewer, AVDynamicTypeSizesSupport
{
	var detailViewController: AVArticleViewController? = nil
	var managedObjectContext: NSManagedObjectContext? = nil
	var model: AVRSSListModel? = nil
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		self.navigationItem.leftBarButtonItem = self.editButtonItem
		self.tableView.estimatedRowHeight = 45
		self.tableView.rowHeight = UITableViewAutomaticDimension

		if let theSplit = self.splitViewController
		{
		    let theControllers = theSplit.viewControllers
		    self.detailViewController = (theControllers[theControllers.count - 1] as!
						UINavigationController).topViewController as? AVArticleViewController
		}
		self.model = AVRSSListModel.init(aManagedObjectContext: self.managedObjectContext)
		self.model?.RSSlistViewer = self
		self.model?.refreshEntitiesList()
	}

	override func viewWillAppear(_ animated: Bool)
	{
		self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
		super.viewWillAppear(animated)
	}

	override func viewDidAppear(_ animated: Bool)
	{
		self.subscribeForFontSizeChanges()
		super.viewDidAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool)
	{
		self.unsubscribeForFontSizeChanges()
		super.viewWillDisappear(animated)
	}

	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
	}

	@IBAction func refresh(_ sender: AnyObject)
	{
		self.model?.refreshEntitiesList()
	}

// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?)
	{
		if segue.identifier == "ShowArticleSegueIdentifier"
		{
			if let theIndexPath = self.tableView.indexPathForSelectedRow
			{
				let theController = (segue.destination as! UINavigationController).topViewController
							as! AVArticleViewController
				if let theItem = self.model?.RSSItem(at: theIndexPath)
				{
					theController.articleURL = URL(string: theItem.articleURL!)
					theController.navigationItem.leftBarButtonItem =
								self.splitViewController?.displayModeButtonItem
					theController.navigationItem.leftItemsSupplementBackButton = true
				}
			}
		}
	}
	
// MARK: - AVDynamicTypeSizesSupport
	
	func didUpdateDynamicTypeSizes()
	{
		self.tableView.reloadData()
	}

// MARK: - Table View

	override func numberOfSections(in tableView: UITableView) -> Int
	{
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return self.model!.entitiesCount()
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
				-> UITableViewCell
	{
		let theCell = tableView.dequeueReusableCell(withIdentifier: "ItemCellIdentifier", for: indexPath)
		if let theItem = self.model?.RSSItem(at: indexPath)
		{
			self.configureCell(theCell, withRSSItem: theItem)
		}
		return theCell
	}

	func configureCell(_ cell: UITableViewCell, withRSSItem item: AVRSSItem)
	{
		cell.textLabel!.text = item.title
		cell.textLabel!.font = UIFont.preferredFont(forTextStyle: .body)
		
		let theDateFormatter = DateFormatter()
		theDateFormatter.dateFormat = "HH:mm:ss, yyyy-MM-dd"
		theDateFormatter.locale = Locale(identifier: "en_US")
		if let date = item.date
		{
			cell.detailTextLabel!.text = theDateFormatter.string(from: date as Date)
		}
		cell.detailTextLabel!.font = UIFont.preferredFont(forTextStyle: .caption1)
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
	{
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle:
				UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
	{
		if editingStyle == .delete
		{
			self.model?.deleteItem(at: indexPath)
		}
	}

// MARK: - AVRSSItemsViewer protocol implementation

	func willStartUpdates()
	{
		self.tableView.beginUpdates()
	}

	func didEndUpdates()
	{
		self.tableView.endUpdates()
	}

	func didOccuredError(_ error: AVRSSItemProviderError)
	{
		let theAlert = UIAlertController(title: "", message: "", preferredStyle: .alert)
		switch error
		{
			case .badRespose:
				theAlert.title = "Server Problem"
				theAlert.message = "Try again later"
			case .badConnection:
				theAlert.title = "Connection Problem"
				theAlert.message = "Check your Internet Connection"
			default:
				theAlert.title = "Could not retrive data"
				theAlert.message = "Try again later"				
		}
		theAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		self.present(theAlert, animated: true, completion: nil)
	}

	func didInsertItem(at indexPath: IndexPath?)
	{
		self.tableView.insertRows(at: [indexPath!], with: .fade)
	}
	
	func didDeleteItem(at indexPath: IndexPath?)
	{
		self.tableView.deleteRows(at: [indexPath!], with: .fade)
	}
	
	func didUpdateItem(at indexPath: IndexPath?)
	{
		self.tableView.reloadRows(at: [indexPath!], with: .fade)
	}
	
	func didMoveItem(at sourceIndexPath: IndexPath?, to destinationIndexPath: IndexPath?)
	{
		self.tableView.moveRow(at: sourceIndexPath!, to: destinationIndexPath!)
	}
}

