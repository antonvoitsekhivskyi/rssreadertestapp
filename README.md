# README #

This is a repository with a simple RSS reader sample. 

Created with Swift 3.0.1, Xcode 8.1.

### Description ###

* Performs https://developer.apple.com/news/rss/news.rss feed parse with XMLParser
* Stores parsed news in CoreData storage
* Article is viewed in UIWebView by selecting one from the articles list
* Implemented support of Dynamic Type Sizes

### By A. Voitsekhivskyi ###